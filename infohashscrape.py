#!/usr/bin/python3
import urllib
import urllib.request
import urllib.parse
import os
import sys
import subprocess
import requests

def search_thepiratebay(term):
    base_link="https://apibay.org"
    url = base_link+"/q.php?q="+term
    
    headers = {"Connection": "close", "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36", "Accept": "*/*", "Sec-Fetch-Site": "same-origin", "Sec-Fetch-Mode": "cors", "Sec-Fetch-Dest": "empty", "Referer": base_link+"/search.php?q=saf", "Accept-Encoding": "gzip, deflate", "Accept-Language": "en-US,en;q=0.9"}
    data = requests.get(url, headers=headers).json()
    return data

def print_infohashes(data):
    for torrent in data:
        print(torrent['info_hash'])
    return
    
def main():
    data = search_thepiratebay(sys.argv[1])
    print_infohashes(data)
    return

if __name__ == "__main__":
    main()
